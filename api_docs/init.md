# Init

First add to your application script with API

```
<script src="/lib/vde.api.js"></script>
```

Next write code to init API

```
(async function () {
    await VDE.init();
})();
```

Api is ready to use