# Test application skeleton

To init app with Vue you need to include scripts

```
<script src="/lib/vde.api.js"></script>
<script src="/lib/vue.prod.js"></script>
<link rel="stylesheet" href="/lib/ui.css">
```

Next you have to write this code

```
(async () => {
    await VDE.initWithVue({
        tabs: ['Tab1', 'Tab2'],
        components: ['test', 'table'],
        start(args) {
            console.log('Hello world', args);
        },
        method: {
            print() {
                            
            }
        },
        options: {
            sortType: {
                type: 'select',
                options: ['none', 'stroke', 'grade'],
                value: 'none'
            },
            gradeFilter: {
                type: 'select',
                options: ['none', 1, 2, 3],
                value: 'none'
            },
            strokeFilter: {
                type: 'select',
                options: ['none', 1, 2, 3, 4, 5],
                value: 'none'
            }
        },
        data: {
            count: 0
        }
    });
})();
```